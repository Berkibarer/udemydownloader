<?php
/********GNU LICENSE***********/
//    This file is part of Udemy Downloader.
//
//    Udemy Downloader is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Udemy Downloader is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Udemy Downloader.  If not, see <http://www.gnu.org/licenses/>.

/********CREDITS*********/
/*12.02.2017 created by Berk Kibarer*/

require_once("UdemyHeaderManager.class.php");

class HTTPRequest
{

    public static function fetch($url, $cookie = true, $post = null, 
                                 $referer = "", $followLocation = false, 
                                 $accessToken = "-", $downloadHeader = false)
    {

        $referer = ($referer == "") ? 
                    UdemyHeaderManager::getHostName() : 
                    $referer;
		
        $header = ($downloadHeader === true) ? 
                   UdemyHeaderManager::getDefaultHeader($referer) : 
                   UdemyHeaderManager::getComplexHeader($accessToken,$referer);

        $cookiePath = UdemyHeaderManager::getCookiePath();

        $ch = curl_init();

        if ($post != null)
        {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        if ($cookie == true)
        {
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiePath);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiePath);
        }

        if ($followLocation == true)
        {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }

        if ($downloadHeader == false)
        {
            curl_setopt($ch, CURLOPT_ENCODING, '');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $result = curl_exec ($ch);

        curl_close ($ch);

        return $result;
    }

	
}

?>
