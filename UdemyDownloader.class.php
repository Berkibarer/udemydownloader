<?php
/********GNU LICENSE***********/
//    This file is part of Udemy Downloader.
//
//    Udemy Downloader is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Udemy Downloader is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Udemy Downloader.  If not, see <http://www.gnu.org/licenses/>.

/********CREDITS*********/
/*12.02.2017 created by Berk Kibarer*/

require_once("HTTPRequest.class.php");

class UdemyDownloader
{

	private $_accessToken = null;
	private $_courseId = null;

	function __construct()
	{

		//Empty cookie file if there is one before you run the script
		if (file_exists(IUdemySettings::cookiePath))
		{
			file_put_contents(IUdemySettings::cookiePath,"");
		}
	}

	private function _getMiddleWareToken()
	{

		$loginContent = HTTPRequest::fetch("https://naspers.udemy.com/",true,null,"",false);

		$parseForCSRF = explode("name='csrfmiddlewaretoken' value='",$loginContent);
		$middlewareToken = substr($parseForCSRF[1],0,strpos($parseForCSRF[1],"'"));

		return $middlewareToken;
	}


	public function loginToUdemy()
	{
		$middlewareToken = $this->_getMiddleWareToken();
		$postData = "locale=en_US&csrfmiddlewaretoken=".$middlewareToken."&email=".IUdemySettings::username;
		$postData .= "&password=".IUdemySettings::password."&displayType=json";
		$referer = 'https://naspers.udemy.com/?next=%2F'.IUdemySettings::courseName.'%2Flearn%2Fv4%2F';
		$url = "https://naspers.udemy.com/join/login-popup/?next=/".IUdemySettings::courseName."/learn/v4/";
		
		HTTPRequest::fetch($url,true,$postData,$referer,true);
	}


	private function _getCourseId()
	{

		$referer = "https://naspers.udemy.com/?next=%2F".IUdemySettings::courseName."%2Flearn%2Fv4%2F";
		$url = "https://naspers.udemy.com/".IUdemySettings::courseName;

		$videoResultPage = HTTPRequest::fetch($url,true,null,$referer,true);

		$videoResultPageExploded = explode(';id&quot;:',$videoResultPage);
		$courseId = trim(substr($videoResultPageExploded[1],0,strpos($videoResultPageExploded[1],",")));
		
		$this->_courseId = $courseId;
	}


	private function _getCookieAccessToken()
	{

		$cookieArr = file(IUdemySettings::cookiePath);
		$accessToken = null;

		foreach($cookieArr as $cookieValuePairs)
		{
			if (strpos($cookieValuePairs, "access_token")>-1)
			{
				$lineArr = explode("access_token", $cookieValuePairs);
				$accessToken= trim($lineArr[1]);
			 }
		}

		$this->_accessToken = $accessToken;

	}


	private function _getCourseDetails()
	{

		$referer = "https://naspers.udemy.com/".IUdemySettings::courseName;
		$urlPart1 = "https://naspers.udemy.com/api-2.0/courses/".$this->_courseId."/";
		$urlPart2 = "cached-subscriber-curriculum-items?fields%5Basset%5D=@min,title,filename";
		$urlPart3 = ",asset_type,external_url,length,status&fields%5Bchapter%5D=@min,description";
		$urlPart4 = ",object_index,title,sort_order&fields%5Blecture%5D=@min,object_index,asset,";
		$urlPart5 = "supplementary_assets,sort_order,is_published,is_free&fields%5Bpractice%5D=";
		$urlPart6 = "@min,object_index,title,sort_order,is_published&fields%5Bquiz%5D=@min,";
		$urlPart7 = "object_index,title,sort_order,is_published&page_size=9999";
		$url = $urlPart1.$urlPart2.$urlPart3.$urlPart4.$urlPart5.$urlPart6.$urlPart7;
		
		$courseHTTPRequest = HTTPRequest::fetch($url, true, null, $referer, 
                                                false, $this->_accessToken, false);

		$courseDetails = json_decode(courseHTTPRequest, true);

		if ($courseDetails["detail"] == "Not found"  || 
		    $courseDetails["detail"] == "You do not have permission to perform this action.")
		{
			throw new Exception("Course Details Not Available!");
		}

		$this->_getDownloadLinks($courseDetails);
	}


	private function _getDownloadLinks($courseDetails)
	{
		
		foreach($courseDetails["results"] as $key => $node){

			if (!isset($node["asset"]))
			{
				continue;
			}

			if (!isset($node["id"]))
			{
				continue;
			}

			if (!isset($node["asset"]["filename"]))
			{
				continue;
			}

			$partConditional = (($key+1)==IUdemySettings::specificCourse 
							|| IUdemySettings::specificCourse=='*');

			if (!$partConditional)
			{
				continue;
			}

			
			//Getting media details for each node
			$referer = "https://naspers.udemy.com/".IUdemySettings::courseName."/v4/t/lecture/".$node["id"];

			$urlPart1 = "https://naspers.udemy.com/api-2.0/users/me/subscribed-courses/";
			$urlPart2 = $this->_courseId."/lectures/".$node["id"]."?fields%5Basset%5D=download_urls";
			$urlPart3 = "&fields%5Blecture%5D=asset";
			$url = $urlPart1.$urlPart2.$urlPart3;

			$courseDetails = HTTPRequest::fetch($url,true,null,$referer,false,$this->_accessToken);

			$downloadJson = json_decode($courseDetails,true);
			$this->_downloadVideo($downloadJson);
		}
	}


	private function _downloadVideo($downloadJson)
	{

		if(!isset($downloadJson["asset"]["download_urls"]["Video"]))
		{
			return;
		}


		foreach($downloadJson["asset"]["download_urls"]["Video"] as $videoNode){
			
			if($videoNode["label"]!=IUdemySettings::resolution)
			{
				continue;
			}
			
			$fileName = str_replace(" ","_",$node["title"])."[".$videoNode["label"]."].mp4";
			echo "File: ".$fileName." Download started...";

			$referer = "https://naspers.udemy.com/".IUdemySettings::courseName."/learn/v4/t/lecture/".$node["id"];
			 
			//SSL Strip
			$url = str_replace("https","http",$videoNode["file"]);
			$fileData = HTTPRequest::fetch($url,true,null,$referer,true,$access_token,true);

			$file = fopen(IUdemySettings::downloadPath.$fileName, "w+");
			fputs($file, $fileData);
			fclose($file);

			echo " || Downloaded. \n";
		}
	}


	public function startDownloading()
	{
            $this->_getCourseId();
            $this->_getCookieAccessToken();
            $this->_getCourseDetails();

	}

}




?>
