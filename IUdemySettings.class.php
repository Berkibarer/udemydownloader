<?php
/********GNU LICENSE***********/
//    This file is part of Udemy Downloader.
//
//    Udemy Downloader is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Udemy Downloader is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Udemy Downloader.  If not, see <http://www.gnu.org/licenses/>.

/********CREDITS*********/
/*12.02.2017 created by Berk Kibarer*/

interface IUdemySettings
{

	const courseName = "wifi-hacking-penetration-testing-from-scratch";
	const hostName = 'naspers.udemy.com';
	const downloadPath = '';
	const resolution = '480'; //360,480,720,1080
	const specificCourse = '*'; //* for all Or any number for nth course video
	const username = "YOUR_EMAIL";
	const password = "YOUR_PASSWORD";
	const cookiePath = 'cookie.txt';
	const header = array(
                    'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0',
                    'Accept-Language: en-US,en;q=0.5',
                    'Connection: keep-alive'
                   );

}

?>