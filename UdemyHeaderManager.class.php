<?php
/********GNU LICENSE***********/
//    This file is part of Udemy Downloader.
//
//    Udemy Downloader is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Udemy Downloader is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Udemy Downloader.  If not, see <http://www.gnu.org/licenses/>.

/********CREDITS*********/
/*12.02.2017 created by Berk Kibarer*/

require_once("IUdemySettings.class.php");

class UdemyHeaderManager implements IUdemySettings
{

	public static function getHostName()
	{
		return IUdemySettings::hostName;
	}


	private static function _getBaseHeader()
	{
		return IUdemySettings::header;
	}

	public static function getCookiePath()
	{
		return IUdemySettings::cookiePath;
	}


	//returns basic header information
	public static function getDefaultHeader($referer)
	{

		$headerBase = array(
                     'Referer: '.$referer,
                     'origin: https://'.self::getHostName()
                      );

		return array_merge(self::_getBaseHeader(),$headerBase);
	}


	//returns additonal headers to default header for more complex requests
	public static function getComplexHeader($accessToken,$referer)
	{	

		$headerAddy = array(
                    'Host: '.self::getHostName(),
                    'Accept: application/json, text/javascript, */*; q=0.01',
                    'Accept-Encoding: gzip, deflate',
                    'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
                    'X-Requested-With: XMLHttpRequest',
                    'Authorization: Bearer '.$accessToken,
                    'X-Udemy-Authorization: Bearer '.$accessToken,
                    );

		return array_merge(self::getDefaultHeader($referer),$headerAddy);
	}

}