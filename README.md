# README #

### What is Udemy Downloader? ###

* This script is generated to download videos from Udemy for any purchased course automatically. As long as you buy the course, you simply can download all videos related to this very course.

* Version : UdemyDownloader-v1.0.0

### Installation, Configuration and Get Going ###

* You need to install php first. 
  Link: http://www.w3schools.com/php/php_install.asp
* You need to install CURL library for PHP since this script highly relies on it. 
  Link: http://askubuntu.com/questions/9293/how-do-i-install-curl-in-php5
* Check IUdemySetting.php and make the needed changes.
* Run startUdemy.php as simple as it sounds.

### Contact ###

* Berk Kibarer
* berkkibarer@gmail.com