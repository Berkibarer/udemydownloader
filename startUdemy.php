<?php
/********GNU LICENSE***********/
//    This file is part of Udemy Downloader.
//
//    Udemy Downloader is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Udemy Downloader is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Udemy Downloader.  If not, see <http://www.gnu.org/licenses/>.

/********CREDITS*********/
/*12.02.2017 created by Berk Kibarer*/

/*
BE SURE THAT YOU MADE THE NEEDED CHANGES 
IN IUdemySettings.class.php file.

THEN SIMPLY RUN THIS FILE AND THAT IS IT.
*/

require_once("UdemyDownloader.class.php");

$UDownloader = new UdemyDownloader();
$UDownloader->loginToUdemy();
$UDownloader->startDownloading();

?>